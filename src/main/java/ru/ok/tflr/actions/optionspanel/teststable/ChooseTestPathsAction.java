package ru.ok.tflr.actions.optionspanel.teststable;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.project.Project;
import ru.ok.tflr.TFLRProjectComponent;
import ru.ok.tflr.toolwindow.OptionsPanel;

import javax.swing.*;

public class ChooseTestPathsAction extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent event) {
        Project project = event.getData(DataKeys.PROJECT);
        if (project == null) return;

        OptionsPanel options = project.getComponent(TFLRProjectComponent.class).getOptionsPanel();
        JTable testPathsTable = options.getTestPathsTable();

        if (testPathsTable.getSelectedRow() < 0) return;
        String name = (String) testPathsTable.getModel().getValueAt(testPathsTable.getSelectedRow(), 0);
        String path = (String) testPathsTable.getModel().getValueAt(testPathsTable.getSelectedRow(), 1);
        options.saveTestPaths(name, path);
    }
}
