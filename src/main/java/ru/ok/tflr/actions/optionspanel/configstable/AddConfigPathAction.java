package ru.ok.tflr.actions.optionspanel.configstable;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.project.Project;
import ru.ok.tflr.TFLRProjectComponent;
import ru.ok.tflr.toolwindow.OptionsPanel;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class AddConfigPathAction extends AnAction {

    private static final Object[] defaultTableRow = new Object[]{"New", ""};

    @Override
    public void actionPerformed(AnActionEvent event) {
        Project project = event.getData(DataKeys.PROJECT);
        if (project == null) return;

        OptionsPanel options = project.getComponent(TFLRProjectComponent.class).getOptionsPanel();
        JTable configsTable = options.getConfigsTable();

        ((DefaultTableModel) configsTable.getModel()).addRow(defaultTableRow);
        options.saveConfigsTable();
    }
}
