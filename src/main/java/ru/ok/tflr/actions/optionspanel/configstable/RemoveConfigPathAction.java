package ru.ok.tflr.actions.optionspanel.configstable;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.project.Project;
import ru.ok.tflr.TFLRProjectComponent;
import ru.ok.tflr.toolwindow.OptionsPanel;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class RemoveConfigPathAction extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent event) {
        Project project = event.getData(DataKeys.PROJECT);
        if (project == null) return;

        OptionsPanel options = project.getComponent(TFLRProjectComponent.class).getOptionsPanel();
        JTable configsTable = options.getConfigsTable();

        if (configsTable.getSelectedRow() < 0) return;
        ((DefaultTableModel) configsTable.getModel()).removeRow(configsTable.getSelectedRow());
        options.saveConfigsTable();
    }
}
