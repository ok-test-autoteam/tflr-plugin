package ru.ok.tflr.actions.optionspanel.configstable;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.project.Project;
import ru.ok.tflr.TFLRProjectComponent;
import ru.ok.tflr.toolwindow.OptionsPanel;

import javax.swing.*;

public class ChooseConfigAction extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent event) {
        Project project = event.getData(DataKeys.PROJECT);
        if (project == null) return;

        OptionsPanel options = project.getComponent(TFLRProjectComponent.class).getOptionsPanel();
        JTable configsTable = options.getConfigsTable();

        if (configsTable.getSelectedRow() < 0) return;
        String name = (String) configsTable.getModel().getValueAt(configsTable.getSelectedRow(), 0);
        String path = (String) configsTable.getModel().getValueAt(configsTable.getSelectedRow(), 1);
        options.saveConfigPath(name, path);
    }
}
