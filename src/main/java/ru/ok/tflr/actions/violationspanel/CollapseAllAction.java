package ru.ok.tflr.actions.violationspanel;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.util.ui.tree.TreeUtil;
import ru.ok.tflr.TFLRProjectComponent;
import ru.ok.tflr.toolwindow.ViolationsPanel;

public class CollapseAllAction extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent event) {
        Project project = event.getData(DataKeys.PROJECT);
        if (project == null) return;

        ViolationsPanel violationsPanel = project.getComponent(TFLRProjectComponent.class).getViolationsPanel();

        TreeUtil.collapseAll(violationsPanel.getTreeViolations(), 2);
    }
}
