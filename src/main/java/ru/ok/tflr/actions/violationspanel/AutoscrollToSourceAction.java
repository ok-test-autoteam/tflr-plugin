package ru.ok.tflr.actions.violationspanel;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.actionSystem.ToggleAction;
import com.intellij.openapi.project.Project;
import ru.ok.tflr.TFLRProjectComponent;
import ru.ok.tflr.config.TFLRConfigurationState;

public class AutoscrollToSourceAction extends ToggleAction {

    @Override
    public boolean isSelected(AnActionEvent e) {
        Project project = e.getData(DataKeys.PROJECT);
        if (project == null) return false;

        TFLRConfigurationState configurationState = project.getComponent(TFLRProjectComponent.class).getConfiguration();
        return configurationState.isAutoscrollToSource();
    }

    @Override
    public void setSelected(AnActionEvent e, boolean state) {
        Project project = e.getData(DataKeys.PROJECT);
        if (project == null) return;

        TFLRConfigurationState configurationState = project.getComponent(TFLRProjectComponent.class).getConfiguration();
        configurationState.setAutoscrollToSource(state);
    }
}
