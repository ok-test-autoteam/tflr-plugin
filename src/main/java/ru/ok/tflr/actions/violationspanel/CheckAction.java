package ru.ok.tflr.actions.violationspanel;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.util.ui.tree.TreeUtil;
import org.jetbrains.annotations.NotNull;
import ru.ok.tflr.TFLRInvoker;
import ru.ok.tflr.TFLRProjectComponent;
import ru.ok.tflr.toolwindow.ViolationsPanel;

public class CheckAction extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent event) {
        Project project = event.getData(DataKeys.PROJECT);
        if (project == null) return;

        ViolationsPanel violationsPanel = project.getComponent(TFLRProjectComponent.class).getViolationsPanel();

        Editor editor = FileEditorManager.getInstance(project).getSelectedTextEditor();
        if (editor == null) return;
        VirtualFile virtualFile = FileDocumentManager.getInstance().getFile(editor.getDocument());
        if (virtualFile == null) return;

        ProgressManager.getInstance().run(new Task.Backgroundable(project, "TFLR Running...") {
            @Override
            public void run(@NotNull ProgressIndicator indicator) {
                indicator.setIndeterminate(true);
                violationsPanel.fillTreeFromResult(TFLRInvoker.runTflrCheckOnCurrentFile(project, virtualFile.getCanonicalPath()));
                ApplicationManager.getApplication().invokeLater(() -> TreeUtil.expandAll(violationsPanel.getTreeViolations()));
            }
        });
    }
}
