package ru.ok.tflr.actions.violationspanel;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;
import ru.ok.tflr.TFLRInvoker;
import ru.ok.tflr.TFLRProjectComponent;
import ru.ok.tflr.toolwindow.ViolationsPanel;

public class CheckAllAction extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent event) {
        Project project = event.getData(DataKeys.PROJECT);
        if (project == null) return;

        ViolationsPanel violationsPanel = project.getComponent(TFLRProjectComponent.class).getViolationsPanel();

        ProgressManager.getInstance().run(new Task.Backgroundable(project, "TFLR Running...", false) {
            @Override
            public void run(@NotNull ProgressIndicator indicator) {
                indicator.setIndeterminate(true);
                violationsPanel.fillTreeFromResult(TFLRInvoker.runTflrCheckOnProject(project));
            }
        });
    }
}
