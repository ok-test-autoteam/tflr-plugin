package ru.ok.tflr.toolwindow;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.openapi.wm.ToolWindowType;
import com.intellij.ui.content.Content;
import org.jetbrains.annotations.NotNull;

public class TFLRToolWindowFactory implements ToolWindowFactory {

    @Override
    public void createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow) {
        Content violationsContent = toolWindow.getContentManager().getFactory().createContent(
                new ViolationsPanel(project),
                "Violations",
                false);
        toolWindow.getContentManager().addContent(violationsContent);

        Content optionsContent = toolWindow.getContentManager().getFactory().createContent(
                new OptionsPanel(project),
                "Options",
                false);
        toolWindow.getContentManager().addContent(optionsContent);

        toolWindow.setType(ToolWindowType.DOCKED, null);
    }
}
