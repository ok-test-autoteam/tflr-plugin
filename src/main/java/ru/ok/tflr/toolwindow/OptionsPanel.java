package ru.ok.tflr.toolwindow;

import com.intellij.openapi.actionSystem.ActionGroup;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.ActionToolbar;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.openapi.util.IconLoader;
import com.intellij.ui.JBColor;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.table.JBTable;
import com.intellij.util.ui.JBUI;
import ru.ok.tflr.TFLRProjectComponent;
import ru.ok.tflr.config.TFLRConfigurationState;
import ru.ok.tflr.reviewer.TFLRRulePriority;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class OptionsPanel extends JPanel {

    public static final String ID_OPTIONS = "TFLROptions";
    private static final String CONFIGS_TABLE_ACTION_GROUP = "TFLRConfigsTableActions";
    private static final String TEST_PATHS_TABLE_ACTION_GROUP = "TFLRTestPathsTableActions";

    private static final Icon CONFIG_ICON = IconLoader.getIcon("/icons/config.png");
    private static final Icon TESTS_ICON = IconLoader.getIcon("/icons/testSourceFolder.png");
    private static final Icon VIO_01_ICON = IconLoader.getIcon("/icons/vio01.png");
    private static final Icon VIO_02_ICON = IconLoader.getIcon("/icons/vio02.png");
    private static final Icon VIO_03_ICON = IconLoader.getIcon("/icons/vio03.png");
    private static final Icon VIO_04_ICON = IconLoader.getIcon("/icons/vio04.png");
    private static final Icon VIO_05_ICON = IconLoader.getIcon("/icons/vio05.png");

    private static final Insets COMPONENT_INSETS = JBUI.insets(4, 7, 4, 4);
    private static final String[] priorityStrings = {TFLRRulePriority.LOW.name(), TFLRRulePriority.MEDIUM_LOW.name(),
            TFLRRulePriority.MEDIUM.name(), TFLRRulePriority.MEDIUM_HIGH.name(), TFLRRulePriority.HIGH.name()};
    private static final Icon[] priorityIcons = {VIO_05_ICON, VIO_04_ICON, VIO_03_ICON, VIO_02_ICON, VIO_01_ICON};

    private static final Object[] columnNames = new String[]{"Name", "Path"};
    private static final Object[][] defaultTableData = new Object[][]{{"Default", ""}};

    private TFLRConfigurationState configurationState;

    private JLabel configNameLabel = new JLabel("Default", SwingConstants.LEFT);
    private JLabel configPathLabel = new JLabel("[empty]", SwingConstants.LEFT);
    private JLabel minimumPriorityLabel = new JLabel("Minimum priority: ");
    private ComboBox<String> priorityComboBox;
    private JCheckBox showRuleNamesCheckBox = new JCheckBox("Show rule names of violations");
    private JLabel testsNameLabel = new JLabel("Default", SwingConstants.LEFT);
    private JLabel testsPathLabel = new JLabel("[empty]", SwingConstants.LEFT);
    private JBTable configsTable = new JBTable(new PathsTableModel(defaultTableData, columnNames));
    private JBTable testPathsTable = new JBTable(new PathsTableModel(defaultTableData, columnNames));

    public OptionsPanel(Project project) {
        super(new BorderLayout());
        this.configurationState = project.getComponent(TFLRProjectComponent.class).getConfiguration();

        priorityComboBox = buildPriorityComboBox();
        showRuleNamesCheckBox.setSelected(configurationState.isRuleNameDisplayed());
        showRuleNamesCheckBox.addChangeListener(e -> configurationState.setRuleNameDisplayed(showRuleNamesCheckBox.isSelected()));

        add(buildMainPanel(), BorderLayout.CENTER);

        loadConfigPath();
        loadTestPaths();
        loadConfigsTable();
        loadTestPathsTable();

        configsTable.getModel().addTableModelListener((modelEvent) -> saveConfigsTable());
        testPathsTable.getModel().addTableModelListener((modelEvent) -> saveTestPathsTable());

        project.getComponent(TFLRProjectComponent.class).setOptionsPanel(this);
    }

    private JPanel buildMainPanel() {
        JPanel mainPanel = new JPanel(new GridBagLayout());

        mainPanel.add(minimumPriorityLabel, new GridBagConstraints(
                0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));
        mainPanel.add(priorityComboBox, new GridBagConstraints(
                1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));
        mainPanel.add(showRuleNamesCheckBox, new GridBagConstraints(
                2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));

        mainPanel.add(new JLabel("Current config: ", CONFIG_ICON, SwingConstants.LEFT), new GridBagConstraints(
                0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));
        mainPanel.add(new JBScrollPane(configNameLabel), new GridBagConstraints(
                1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));
        mainPanel.add(new JBScrollPane(configPathLabel), new GridBagConstraints(
                2, 1, 1, 1, 3.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));

        mainPanel.add(buildPathsTable(configsTable, CONFIGS_TABLE_ACTION_GROUP), new GridBagConstraints(
                0, 2, 3, 1, 1.0, 1.0, GridBagConstraints.WEST,
                GridBagConstraints.BOTH, JBUI.insets(2), 0, 0));

        mainPanel.add(new JLabel("Current path to all tests: ", TESTS_ICON, SwingConstants.LEFT), new GridBagConstraints(
                0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.NONE, COMPONENT_INSETS, 0, 0));
        mainPanel.add(new JBScrollPane(testsNameLabel), new GridBagConstraints(
                1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));
        mainPanel.add(new JBScrollPane(testsPathLabel), new GridBagConstraints(
                2, 3, 1, 1, 3.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.HORIZONTAL, COMPONENT_INSETS, 0, 0));

        mainPanel.add(buildPathsTable(testPathsTable, TEST_PATHS_TABLE_ACTION_GROUP), new GridBagConstraints(
                0, 4, 3, 1, 1.0, 1.0, GridBagConstraints.WEST,
                GridBagConstraints.BOTH, JBUI.insets(2), 0, 0));

        return mainPanel;
    }

    private ComboBox<String> buildPriorityComboBox() {
        ComboBox<String> newComboBox = new ComboBox<>();
        newComboBox.setModel(new DefaultComboBoxModel<>(priorityStrings));
        newComboBox.setSelectedItem(configurationState.getMinimumPriority().name());
        minimumPriorityLabel.setIcon(priorityIcons[Arrays.asList(priorityStrings).indexOf(configurationState.getMinimumPriority().name())]);
        newComboBox.addActionListener((actionEvent) -> {
            ComboBox comboBox = (ComboBox) actionEvent.getSource();
            configurationState.setMinimumPriority(TFLRRulePriority.valueOf((String) comboBox.getSelectedItem()));
            minimumPriorityLabel.setIcon(priorityIcons[comboBox.getSelectedIndex()]);
        });
        return newComboBox;
    }

    private JPanel buildPathsTable(JBTable table, String actionGroup) {
        setColumnWidth(table, 0, 100, 300, 300);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        table.setStriped(true);
        table.getTableHeader().setReorderingAllowed(false);
        table.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        ActionGroup optionsActionGroup = (ActionGroup) ActionManager.getInstance().getAction(actionGroup);
        ActionToolbar optionsToolbar = ActionManager.getInstance().createActionToolbar(ID_OPTIONS, optionsActionGroup, false);
        optionsToolbar.getComponent().setBorder(BorderFactory.createLineBorder(JBColor.border()));
        optionsToolbar.getComponent().setVisible(true);

        JPanel container = new JPanel(new BorderLayout());
        container.add(optionsToolbar.getComponent(), BorderLayout.WEST);
        container.add(new JBScrollPane(table), BorderLayout.CENTER);
        return container;
    }

    private void setColumnWidth(JTable table, int columnIndex, int minSize, int preferredSize, Integer maxSize) {
        TableColumn column = table.getColumnModel().getColumn(columnIndex);
        column.setMinWidth(minSize);
        column.setWidth(preferredSize);
        column.setPreferredWidth(preferredSize);
        if (maxSize != null) {
            column.setMaxWidth(maxSize);
        }
    }

    public void saveConfigPath(String name, String path) {
        configNameLabel.setText(name);
        configNameLabel.setToolTipText(name);
        configPathLabel.setText(path);
        configPathLabel.setToolTipText(path);
        configurationState.setConfigName(name);
        configurationState.setConfigPath(path);
    }

    private void loadConfigPath() {
        String name = configurationState.getConfigName();
        String path = configurationState.getConfigPath();
        configNameLabel.setText(name);
        configNameLabel.setToolTipText(name);
        configPathLabel.setText(path);
        configPathLabel.setToolTipText(path);
    }

    public void saveTestPaths(String name, String path) {
        testsNameLabel.setText(name);
        testsNameLabel.setToolTipText(name);
        testsPathLabel.setText(path);
        testsPathLabel.setToolTipText(path);
        configurationState.setTestFilesName(name);
        configurationState.setTestFilesPath(path);
    }

    private void loadTestPaths() {
        String name = configurationState.getTestFilesName();
        String path = configurationState.getTestFilesPath();
        testsNameLabel.setText(name);
        testsNameLabel.setToolTipText(name);
        testsPathLabel.setText(path);
        testsPathLabel.setToolTipText(path);
    }

    public void saveConfigsTable() {
        configurationState.setContentOfConfigsTable(new LinkedHashMap<>());
        for (int i = 0; i < configsTable.getRowCount(); i++) {
            configurationState.addConfigToTable((String) configsTable.getModel().getValueAt(i, 0),
                    (String) configsTable.getModel().getValueAt(i, 1));
        }
        configurationState.addConfigToTable("Default", "");
    }

    private void loadConfigsTable() {
        ((PathsTableModel) configsTable.getModel()).getDataVector().clear();

        Map<String, String> contentOfTable = configurationState.getContentOfConfigsTable();
        for (String name : contentOfTable.keySet()) {
            ((PathsTableModel) configsTable.getModel()).addRow(new Object[]{name, contentOfTable.get(name)});
        }
    }

    public void saveTestPathsTable() {
        configurationState.setContentOfTestPathsTable(new LinkedHashMap<>());
        for (int i = 0; i < testPathsTable.getRowCount(); i++) {
            configurationState.addTestPathToTable((String) testPathsTable.getModel().getValueAt(i, 0),
                    (String) testPathsTable.getModel().getValueAt(i, 1));
        }
        configurationState.addTestPathToTable("Default", "");
    }

    private void loadTestPathsTable() {
        ((PathsTableModel) testPathsTable.getModel()).getDataVector().clear();

        Map<String, String> contentOfTable = configurationState.getContentOfTestPathsTable();
        for (String name : contentOfTable.keySet()) {
            ((PathsTableModel) testPathsTable.getModel()).addRow(new Object[]{name, contentOfTable.get(name)});
        }
    }

    public JTable getConfigsTable() {
        return configsTable;
    }

    public JTable getTestPathsTable() {
        return testPathsTable;
    }

    private class PathsTableModel extends DefaultTableModel {

        PathsTableModel(Object[][] data, Object[] columnNames) {
            super(data, columnNames);
        }

        @Override
        public boolean isCellEditable(int row, int column) {
            return true;
        }
    }
}
