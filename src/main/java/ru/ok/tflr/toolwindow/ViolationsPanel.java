package ru.ok.tflr.toolwindow;

import com.intellij.openapi.actionSystem.ActionGroup;
import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.ActionToolbar;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.OpenFileDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.ui.treeStructure.Tree;
import ru.ok.tflr.TFLRProjectComponent;
import ru.ok.tflr.config.TFLRConfigurationState;
import ru.ok.tflr.reviewer.TFLRResult;
import ru.ok.tflr.reviewer.TFLRStatus;
import ru.ok.tflr.tree.TFLRCellRenderer;
import ru.ok.tflr.tree.TFLRRootNode;
import ru.ok.tflr.tree.TFLRTreeNodeFactory;
import ru.ok.tflr.tree.TFLRViolationNode;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Comparator;

public class ViolationsPanel extends JPanel {

    private static final String TOOL_WINDOW_ACTION_GROUP = "TFLRToolWindowActions";
    private static final TFLRTreeNodeFactory TREE_NODE_FACTORY = TFLRTreeNodeFactory.getInstance();

    private Project project;
    private TFLRConfigurationState configurationState;
    private TFLRRootNode rootNode;

    private Tree treeViolations;

    public ViolationsPanel(Project project) {
        super(new BorderLayout());
        this.project = project;
        this.configurationState = project.getComponent(TFLRProjectComponent.class).getConfiguration();

        ActionGroup toolWindowActionGroup = (ActionGroup) ActionManager.getInstance().getAction(TOOL_WINDOW_ACTION_GROUP);
        ActionToolbar toolWindowToolbar = ActionManager.getInstance().createActionToolbar("Violations", toolWindowActionGroup, false);
        toolWindowToolbar.getComponent().setVisible(true);
        add(toolWindowToolbar.getComponent(), BorderLayout.WEST);

        this.treeViolations = createNewTflrTree();
        add(new JBScrollPane(treeViolations), BorderLayout.CENTER);

        //Add mouse listener to support double click.
        treeViolations.addMouseListener(new MouseAdapter() {
            //Get the current tree node where the mouse event happened
            private DefaultMutableTreeNode[] getNodeFromEvent() {
                TreePath[] selectionPaths = treeViolations.getSelectionPaths();
                if (selectionPaths == null) return null;

                DefaultMutableTreeNode[] result = new DefaultMutableTreeNode[selectionPaths.length];
                for (int i = 0; i < result.length; i++) {
                    result[i] = (DefaultMutableTreeNode) selectionPaths[i].getLastPathComponent();
                }
                return result;
            }

            @Override
            public void mousePressed(MouseEvent e) {
                DefaultMutableTreeNode[] treeNodes = getNodeFromEvent();
                if (treeNodes == null) return;
                if (configurationState.isAutoscrollToSource() || e.getClickCount() == 2) {
                    for (DefaultMutableTreeNode treeNode : treeNodes) {
                        highlightError(treeNode);
                    }
                }
            }
        });

        project.getComponent(TFLRProjectComponent.class).setViolationsPanel(this);
    }

    /**
     * Creates the tree which will display violations.
     *
     * @return new tree which will display violations
     */
    private Tree createNewTflrTree() {
        Tree newTree = new Tree();
        rootNode = (TFLRRootNode) TREE_NODE_FACTORY.createDefaultRootNode();
        TreeModel treeModel = new DefaultTreeModel(rootNode);
        newTree.setModel(treeModel);
        newTree.setCellRenderer(new TFLRCellRenderer(project.getComponent(TFLRProjectComponent.class)));
        return newTree;
    }

    /**
     * Highlights a given violation/error represented by the given tree node.
     *
     * @param treeNode The tree node having the violation
     */
    private void highlightError(DefaultMutableTreeNode treeNode) {
        if (treeNode instanceof TFLRViolationNode) {
            openEditor((TFLRViolationNode) treeNode);
        }
    }

    /**
     * Opens the given violation's file in the Editor and returns the Editor.
     *
     * @param violationNode The Violation
     * @return the editor with caret at the violation
     */
    private Editor openEditor(TFLRViolationNode violationNode) {
        FileEditorManager fileEditorManager = FileEditorManager.getInstance(project);
        final VirtualFile virtualFile = LocalFileSystem.getInstance().findFileByPath(
                violationNode.getRuleViolation().getFilename().replace(File.separatorChar, '/'));
        if (virtualFile != null) {
            return fileEditorManager.openTextEditor(new OpenFileDescriptor(
                            project,
                            virtualFile,
                            Math.max(violationNode.getRuleViolation().getBeginLine() - 1, 0),
                            Math.max(violationNode.getRuleViolation().getBeginColumn() - 1, 0)),
                    true);
        }
        return null;
    }

    public void fillTreeFromResult(TFLRResult tflrResult) {
        rootNode.removeAllChildren();
        rootNode.setTflrResult(tflrResult);
        if (tflrResult.getStatus() == TFLRStatus.VIOLATIONS_FOUND) {
            tflrResult.getReports().forEach((fileName, report) -> {
                DefaultMutableTreeNode reportNode = addNode(TREE_NODE_FACTORY.createNode(fileName, report));
                report.getViolations().sort(Comparator.comparingInt(o -> o.getRule().getPriority().getPriorityId()));
                report.forEach((ruleViolation) -> addNode(reportNode, TREE_NODE_FACTORY.createNode(ruleViolation)));
            });
        } else if (tflrResult.getStatus() == TFLRStatus.SUCCESS) {
            addNode(TREE_NODE_FACTORY.createNode("All checks successfully passed!"));
        } else {
            addNode(TREE_NODE_FACTORY.createNode("There are some errors! " + tflrResult.getStatus().name()));
        }
    }

    /**
     * Adds a node to the tree as a direct child of the root, and return it
     *
     * @param node the user object of the tree node to create
     * @return the created node
     */
    private DefaultMutableTreeNode addNode(DefaultMutableTreeNode node) {
        return addNode(rootNode, node);
    }

    /**
     * Adds a node to the given node as parent.
     *
     * @param parent The parent node
     * @param node   The child not which is added as child to parent node
     * @return the child node
     */
    private DefaultMutableTreeNode addNode(DefaultMutableTreeNode parent, DefaultMutableTreeNode node) {
        parent.add(node);
        ApplicationManager.getApplication().invokeLater(() -> ((DefaultTreeModel) treeViolations.getModel()).reload());
        return node;
    }

    public Tree getTreeViolations() {
        return treeViolations;
    }
}
