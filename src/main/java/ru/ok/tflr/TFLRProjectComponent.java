package ru.ok.tflr;

import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import ru.ok.tflr.config.TFLRConfigurationState;
import ru.ok.tflr.toolwindow.OptionsPanel;
import ru.ok.tflr.toolwindow.ViolationsPanel;

public class TFLRProjectComponent implements ProjectComponent {

    /**
     * The plugin ID. Caution: It must be identical to the String set in build.gradle at intellij.pluginName
     */
    public static final String ID_PLUGIN = "TFLR.Plugin";

    private final Project project;
    private final TFLRConfigurationState configuration;
    private ViolationsPanel violationsPanel;
    private OptionsPanel optionsPanel;

    public TFLRProjectComponent(Project project) {
        this.project = project;
        this.configuration = ServiceManager.getService(project, TFLRConfigurationState.class);
    }

    public Project getProject() {
        return project;
    }

    public TFLRConfigurationState getConfiguration() {
        return configuration;
    }

    public ViolationsPanel getViolationsPanel() {
        return violationsPanel;
    }

    public void setViolationsPanel(ViolationsPanel violationsPanel) {
        this.violationsPanel = violationsPanel;
    }

    public OptionsPanel getOptionsPanel() {
        return optionsPanel;
    }

    public void setOptionsPanel(OptionsPanel optionsPanel) {
        this.optionsPanel = optionsPanel;
    }
}
