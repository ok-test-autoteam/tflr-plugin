package ru.ok.tflr.config;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class TFLRProjectSettings {

    public static final String MIN_PRIORITY = "MinimumPriority";
    public static final String IS_RULE_NAME_DISPLAYED = "isRuleNameDisplayed";
    public static final String IS_AUTOSCROLL_TO_SOURCE = "isAutoscrollToSource";
    public static final String CONFIG_NAME = "ConfigName";
    public static final String CONFIG_PATH = "ConfigPath";
    public static final String TEST_FILES_NAME = "TestFilesName";
    public static final String TEST_FILES_PATH = "TestFilesPath";

    private Map<String, String> options;                    // <option name, value>
    private Map<String, String> contentOfConfigsTable;      // <name, path>
    private Map<String, String> contentOfTestPathsTable;    // <name, path>

    public TFLRProjectSettings() {
        this.options = new HashMap<>();
        this.contentOfConfigsTable = new LinkedHashMap<>();
        this.contentOfTestPathsTable = new LinkedHashMap<>();
    }

    public String getOption(String optionName) {
        return options.get(optionName);
    }

    public void setOption(String optionName, String value) {
        options.put(optionName, value);
    }

    public void addConfigToTable(String name, String path) {
        contentOfConfigsTable.put(name, path);
    }

    public void addTestPathToTable(String name, String path) {
        contentOfTestPathsTable.put(name, path);
    }

    public Map<String, String> getOptions() {
        return options;
    }

    public void setOptions(Map<String, String> options) {
        this.options = options;
    }

    public Map<String, String> getContentOfConfigsTable() {
        return contentOfConfigsTable;
    }

    public void setContentOfConfigsTable(Map<String, String> contentOfConfigsTable) {
        this.contentOfConfigsTable = contentOfConfigsTable;
    }

    public Map<String, String> getContentOfTestPathsTable() {
        return contentOfTestPathsTable;
    }

    public void setContentOfTestPathsTable(Map<String, String> contentOfTestPathsTable) {
        this.contentOfTestPathsTable = contentOfTestPathsTable;
    }
}
