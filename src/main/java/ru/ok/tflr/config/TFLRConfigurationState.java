package ru.ok.tflr.config;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import ru.ok.tflr.TFLRProjectComponent;
import ru.ok.tflr.reviewer.TFLRRulePriority;

import java.util.Map;

@State(name = TFLRProjectComponent.ID_PLUGIN, storages = {@Storage("tflr-plugin.xml")})
public class TFLRConfigurationState implements PersistentStateComponent<TFLRProjectSettings> {

    private TFLRProjectSettings projectSettings;

    public TFLRConfigurationState() {
        projectSettings = defaultProjectSettings();
    }

    @Override
    public TFLRProjectSettings getState() {
        return projectSettings;
    }

    @Override
    public void loadState(TFLRProjectSettings state) {
        if (state != null) {
            projectSettings = state;
        }
    }

    private TFLRProjectSettings defaultProjectSettings() {
        TFLRProjectSettings defaultProjectSettings = new TFLRProjectSettings();
        defaultProjectSettings.setOption(TFLRProjectSettings.MIN_PRIORITY, "LOW");
        defaultProjectSettings.setOption(TFLRProjectSettings.IS_RULE_NAME_DISPLAYED, Boolean.toString(true));
        defaultProjectSettings.setOption(TFLRProjectSettings.IS_AUTOSCROLL_TO_SOURCE, Boolean.toString(false));
        defaultProjectSettings.setOption(TFLRProjectSettings.CONFIG_NAME, "Default");
        defaultProjectSettings.setOption(TFLRProjectSettings.CONFIG_PATH, "");
        defaultProjectSettings.setOption(TFLRProjectSettings.TEST_FILES_NAME, "Default");
        defaultProjectSettings.setOption(TFLRProjectSettings.TEST_FILES_PATH, "");
        defaultProjectSettings.addConfigToTable("Default", "");
        defaultProjectSettings.addTestPathToTable("Default", "");
        return defaultProjectSettings;
    }

    public boolean isRuleNameDisplayed() {
        return Boolean.valueOf(projectSettings.getOption(TFLRProjectSettings.IS_RULE_NAME_DISPLAYED));
    }

    public void setRuleNameDisplayed(boolean isRuleNameDisplayed) {
        projectSettings.setOption(TFLRProjectSettings.IS_RULE_NAME_DISPLAYED, String.valueOf(isRuleNameDisplayed));
    }

    public TFLRRulePriority getMinimumPriority() {
        return TFLRRulePriority.valueOf(projectSettings.getOption(TFLRProjectSettings.MIN_PRIORITY));
    }

    public void setMinimumPriority(TFLRRulePriority minimumPriority) {
        projectSettings.setOption(TFLRProjectSettings.MIN_PRIORITY, minimumPriority.name());
    }

    public boolean isAutoscrollToSource() {
        return Boolean.valueOf(projectSettings.getOption(TFLRProjectSettings.IS_AUTOSCROLL_TO_SOURCE));
    }

    public void setAutoscrollToSource(boolean autoscrollToSource) {
        projectSettings.setOption(TFLRProjectSettings.IS_AUTOSCROLL_TO_SOURCE, String.valueOf(autoscrollToSource));
    }

    public String getConfigName() {
        return projectSettings.getOption(TFLRProjectSettings.CONFIG_NAME);
    }

    public void setConfigName(String configName) {
        projectSettings.setOption(TFLRProjectSettings.CONFIG_NAME, configName);
    }

    public String getConfigPath() {
        return projectSettings.getOption(TFLRProjectSettings.CONFIG_PATH);
    }

    public void setConfigPath(String configPath) {
        projectSettings.setOption(TFLRProjectSettings.CONFIG_PATH, configPath);
    }

    public String getTestFilesName() {
        return projectSettings.getOption(TFLRProjectSettings.TEST_FILES_NAME);
    }

    public void setTestFilesName(String testFilesName) {
        projectSettings.setOption(TFLRProjectSettings.TEST_FILES_NAME, testFilesName);
    }

    public String getTestFilesPath() {
        return projectSettings.getOption(TFLRProjectSettings.TEST_FILES_PATH);
    }

    public void setTestFilesPath(String testFilesPath) {
        projectSettings.setOption(TFLRProjectSettings.TEST_FILES_PATH, testFilesPath);
    }

    public Map<String, String> getContentOfConfigsTable() {
        return projectSettings.getContentOfConfigsTable();
    }

    public void setContentOfConfigsTable(Map<String, String> contentOfConfigsTable) {
        projectSettings.setContentOfConfigsTable(contentOfConfigsTable);
    }

    public Map<String, String> getContentOfTestPathsTable() {
        return projectSettings.getContentOfTestPathsTable();
    }

    public void setContentOfTestPathsTable(Map<String, String> contentOfTestPathsTable) {
        projectSettings.setContentOfTestPathsTable(contentOfTestPathsTable);
    }

    public void addConfigToTable(String name, String path) {
        projectSettings.addConfigToTable(name, path);
    }

    public void addTestPathToTable(String name, String path) {
        projectSettings.addTestPathToTable(name, path);
    }
}
