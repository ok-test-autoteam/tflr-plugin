package ru.ok.tflr.tree;

import com.intellij.ui.SimpleTextAttributes;

import javax.swing.tree.DefaultMutableTreeNode;

public class TFLRInfoNode extends DefaultMutableTreeNode implements TFLRTreeNodeData  {

    private String text;

    TFLRInfoNode(String text) {
        this.text = text;
    }

    @Override
    public void render(TFLRCellRenderer cellRenderer) {
        cellRenderer.append(text, SimpleTextAttributes.REGULAR_ATTRIBUTES);
    }
}
