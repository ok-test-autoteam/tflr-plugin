package ru.ok.tflr.tree;

import com.intellij.openapi.util.IconLoader;
import com.intellij.ui.ColoredTreeCellRenderer;
import org.jetbrains.annotations.NotNull;
import ru.ok.tflr.TFLRProjectComponent;

import javax.swing.*;

public class TFLRCellRenderer extends ColoredTreeCellRenderer {

    static final Icon VIO_01_ICON = IconLoader.getIcon("/icons/vio01.png");
    static final Icon VIO_02_ICON = IconLoader.getIcon("/icons/vio02.png");
    static final Icon VIO_03_ICON = IconLoader.getIcon("/icons/vio03.png");
    static final Icon VIO_04_ICON = IconLoader.getIcon("/icons/vio04.png");
    static final Icon VIO_05_ICON = IconLoader.getIcon("/icons/vio05.png");
    static final Icon CLASS_VIOLATIONS_ICON = IconLoader.getIcon("/icons/sources.png");
    static final Icon CHECKOUT_SUCCESS_ICON = IconLoader.getIcon("/icons/testPassed.png");
    static final Icon CHECKOUT_VIOLATION_ICON = IconLoader.getIcon("/icons/testFailed.png");
    static final Icon CHECKOUT_ERROR_ICON = IconLoader.getIcon("/icons/testError.png");

    private TFLRProjectComponent projectComponent;

    public TFLRCellRenderer(TFLRProjectComponent projectComponent) {
        super();
        this.projectComponent = projectComponent;
    }

    @Override
    public void customizeCellRenderer(@NotNull JTree tree,
                                      Object value,
                                      boolean selected,
                                      boolean expanded,
                                      boolean leaf,
                                      int row,
                                      boolean hasFocus) {
        if (value instanceof TFLRTreeNodeData) {
            ((TFLRTreeNodeData) value).render(this);
        }
    }

    public boolean isRuleNameDisplayed() {
        return projectComponent.getConfiguration().isRuleNameDisplayed();
    }
}
