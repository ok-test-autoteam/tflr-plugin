package ru.ok.tflr.tree;

public interface TFLRTreeNodeData {

    /**
     * Render this node data using the cell renderer given.
     *
     * @param cellRenderer The Cell Renderer to render this node data.
     */
    void render(TFLRCellRenderer cellRenderer);
}
