package ru.ok.tflr.tree;

import com.intellij.ui.SimpleTextAttributes;
import ru.ok.tflr.reviewer.TFLRResult;
import ru.ok.tflr.reviewer.TFLRStatus;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.util.HashMap;
import java.util.Map;

public class TFLRRootNode extends DefaultMutableTreeNode implements TFLRTreeNodeData {

    /**
     * Label of the root node.
     */
    private static final String LABEL = "Test First Line Review Results";

    private TFLRResult tflrResult;

    TFLRRootNode() {
        super(LABEL);
    }

    public TFLRRootNode(String nodeName) {
        super(nodeName);
    }

    TFLRRootNode(TFLRResult tflrResult) {
        this.tflrResult = tflrResult;
    }

    @Override
    public void render(TFLRCellRenderer cellRenderer) {
        cellRenderer.append(LABEL, SimpleTextAttributes.REGULAR_ATTRIBUTES);
        if (tflrResult != null) {
            cellRenderer.setIcon(RESULT_ICONS.get(tflrResult.getStatus().name()));
            cellRenderer.append(" (" + String.valueOf(tflrResult.getReports().size()) + " files with violations)",
                    SimpleTextAttributes.GRAYED_ATTRIBUTES);
        } else cellRenderer.setIcon(TFLRCellRenderer.CHECKOUT_SUCCESS_ICON);
    }

    private static final Map<String, Icon> RESULT_ICONS = new HashMap<>();

    static {
        RESULT_ICONS.put(TFLRStatus.SUCCESS.name(), TFLRCellRenderer.CHECKOUT_SUCCESS_ICON);
        RESULT_ICONS.put(TFLRStatus.VIOLATIONS_FOUND.name(), TFLRCellRenderer.CHECKOUT_VIOLATION_ICON);
        RESULT_ICONS.put(TFLRStatus.ERROR.name(), TFLRCellRenderer.CHECKOUT_ERROR_ICON);
        RESULT_ICONS.put(TFLRStatus.NO_CHECKS_WERE_RUN.name(), TFLRCellRenderer.CHECKOUT_ERROR_ICON);
        RESULT_ICONS.put(TFLRStatus.WRONG_CONFIG.name(), TFLRCellRenderer.CHECKOUT_ERROR_ICON);
    }

    public TFLRResult getTflrResult() {
        return tflrResult;
    }

    public void setTflrResult(TFLRResult tflrResult) {
        this.tflrResult = tflrResult;
    }
}
