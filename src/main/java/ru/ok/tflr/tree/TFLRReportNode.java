package ru.ok.tflr.tree;

import com.intellij.ui.SimpleTextAttributes;
import ru.ok.tflr.reviewer.TFLRReport;
import ru.ok.tflr.reviewer.TFLRRuleViolation;

import javax.swing.tree.DefaultMutableTreeNode;

public class TFLRReportNode extends DefaultMutableTreeNode implements TFLRTreeNodeData {

    private String fileName;
    private TFLRReport tflrReport;
    private String className;

    TFLRReportNode(String fileName, TFLRReport report) {
        this.fileName = fileName;
        this.tflrReport = report;
        TFLRRuleViolation violation = report.iterator().next();
        className = violation.getPackageName() + "." + violation.getClassName();
    }

    @Override
    public void render(TFLRCellRenderer cellRenderer) {
        cellRenderer.setIcon(TFLRCellRenderer.CLASS_VIOLATIONS_ICON);
        cellRenderer.append(className, SimpleTextAttributes.REGULAR_ATTRIBUTES);
        cellRenderer.append(" (" + String.valueOf(tflrReport.getViolations().size()) + " violations) ",
                SimpleTextAttributes.GRAYED_ATTRIBUTES);
    }
}
