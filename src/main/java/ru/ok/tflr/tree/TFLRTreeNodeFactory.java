package ru.ok.tflr.tree;

import ru.ok.tflr.reviewer.TFLRReport;
import ru.ok.tflr.reviewer.TFLRResult;
import ru.ok.tflr.reviewer.TFLRRuleViolation;

import javax.swing.tree.DefaultMutableTreeNode;

public class TFLRTreeNodeFactory {

    private static TFLRTreeNodeFactory ourInstance = new TFLRTreeNodeFactory();

    private TFLRTreeNodeFactory() {
    }

    public static TFLRTreeNodeFactory getInstance() {
        return ourInstance;
    }

    public DefaultMutableTreeNode createNode(Object userObject) {
        if (userObject instanceof TFLRRuleViolation) {
            return new TFLRViolationNode((TFLRRuleViolation) userObject);
        } else if (userObject instanceof TFLRResult) {
            return new TFLRRootNode((TFLRResult) userObject);
        } else if (userObject instanceof String) {
            return new TFLRInfoNode((String) userObject);
        }
        return null;
    }

    public DefaultMutableTreeNode createNode(String fileName, TFLRReport report) {
        return new TFLRReportNode(fileName, report);
    }

    public DefaultMutableTreeNode createDefaultRootNode() {
        return new TFLRRootNode();
    }
}
