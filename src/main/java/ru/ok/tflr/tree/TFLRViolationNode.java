package ru.ok.tflr.tree;

import com.intellij.ui.SimpleTextAttributes;
import ru.ok.tflr.reviewer.TFLRRulePriority;
import ru.ok.tflr.reviewer.TFLRRuleViolation;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.util.HashMap;
import java.util.Map;

public class TFLRViolationNode extends DefaultMutableTreeNode implements TFLRTreeNodeData {

    private TFLRRuleViolation ruleViolation;
    private String errorPosition;

    TFLRViolationNode(TFLRRuleViolation ruleViolation) {
        this.ruleViolation = ruleViolation;
        this.errorPosition = "(" + ruleViolation.getBeginLine() + ", " + ruleViolation.getBeginColumn() + ") ";
    }

    @Override
    public void render(TFLRCellRenderer cellRenderer) {
        cellRenderer.setIcon(PRIORITY_ICONS.get(getRuleViolation().getRule().getPriority().name()));
        cellRenderer.append(getErrorPosition(), SimpleTextAttributes.GRAYED_ATTRIBUTES);
        cellRenderer.append(ruleViolation.getMessage(), SimpleTextAttributes.REGULAR_ATTRIBUTES);
        if (cellRenderer.isRuleNameDisplayed() && ruleViolation.getRule() != null)
            cellRenderer.append("  [" + ruleViolation.getRule().getName() + "]", SimpleTextAttributes.GRAYED_ATTRIBUTES);
    }

    private static final Map<String, Icon> PRIORITY_ICONS = new HashMap<>();

    static {
        PRIORITY_ICONS.put(TFLRRulePriority.HIGH.name(), TFLRCellRenderer.VIO_01_ICON);
        PRIORITY_ICONS.put(TFLRRulePriority.MEDIUM_HIGH.name(), TFLRCellRenderer.VIO_02_ICON);
        PRIORITY_ICONS.put(TFLRRulePriority.MEDIUM.name(), TFLRCellRenderer.VIO_03_ICON);
        PRIORITY_ICONS.put(TFLRRulePriority.MEDIUM_LOW.name(), TFLRCellRenderer.VIO_04_ICON);
        PRIORITY_ICONS.put(TFLRRulePriority.LOW.name(), TFLRCellRenderer.VIO_05_ICON);
    }

    public TFLRRuleViolation getRuleViolation() {
        return ruleViolation;
    }

    public void setRuleViolation(TFLRRuleViolation ruleViolation) {
        this.ruleViolation = ruleViolation;
    }

    private String getErrorPosition() {
        return errorPosition;
    }

    public void setErrorPosition(String errorPosition) {
        this.errorPosition = errorPosition;
    }
}
