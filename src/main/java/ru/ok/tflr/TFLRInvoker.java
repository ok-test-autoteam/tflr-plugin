package ru.ok.tflr;

import com.intellij.openapi.project.Project;
import ru.ok.tflr.config.TFLRConfigurationState;
import ru.ok.tflr.reviewer.TFLRResult;
import ru.ok.tflr.reviewer.TFLRRulePriority;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class TFLRInvoker {

    private static final String OPTIONS_PATH_DELIMITER = ",";
    private static final String BASE_DIRECTORY_DELIMITER = "://";
    private static final String PATH_DELIMITER = File.separator;
    private static final String PATH_TO_TESTS = File.separator + "src";
    private static final String TFLR_CONFIG_YML = "tflr-config.yml";

    private TFLRInvoker() {
    }

    public static TFLRResult runTflrCheckOnCurrentFile(Project project, String filePath) {
        TFLRConfigurationState configurationState = project.getComponent(TFLRProjectComponent.class).getConfiguration();
        String defaultConfigPath = getDisplayedPath(project.getBaseDir().toString()) + PATH_DELIMITER + TFLR_CONFIG_YML;

        TFLRResult result = new TFLRResult();

        List<String> configPaths = configurationState.getConfigPath().isEmpty() ? Collections.singletonList(defaultConfigPath)
                : Arrays.asList(configurationState.getConfigPath().split(OPTIONS_PATH_DELIMITER));
        TFLRRulePriority minPriority = configurationState.getMinimumPriority();

        configPaths.forEach((s) -> result.merge(TFLR.runTFLR(Collections.singletonList(filePath), s, minPriority)));
        return result;
    }

    public static TFLRResult runTflrCheckOnProject(Project project) {
        TFLRConfigurationState configurationState = project.getComponent(TFLRProjectComponent.class).getConfiguration();
        String projectPath = getDisplayedPath(project.getBaseDir().toString());
        String defaultConfigPath = projectPath + PATH_DELIMITER + TFLR_CONFIG_YML;

        TFLRResult result = new TFLRResult();

        List<String> paths = configurationState.getTestFilesPath().isEmpty() ? Collections.singletonList(projectPath + PATH_TO_TESTS)
                : Arrays.asList(configurationState.getTestFilesPath().split(OPTIONS_PATH_DELIMITER));
        List<String> configPaths = configurationState.getConfigPath().isEmpty() ? Collections.singletonList(defaultConfigPath)
                : Arrays.asList(configurationState.getConfigPath().split(OPTIONS_PATH_DELIMITER));
        TFLRRulePriority minPriority = configurationState.getMinimumPriority();

        configPaths.forEach((s) -> result.merge(TFLR.runTFLR(paths, s, minPriority)));
        return result;
    }

    private static String getDisplayedPath(String uri) {
        final int firstDelimiterIndex = uri.indexOf(BASE_DIRECTORY_DELIMITER);
        return (firstDelimiterIndex != -1) ?
                uri.substring(firstDelimiterIndex + BASE_DIRECTORY_DELIMITER.length()) :
                uri;
    }
}
