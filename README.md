# Test First Line Review Plugin

Plugin for [Intellij Idea IDE](https://www.jetbrains.com/idea/ "Intellij Idea IDE"), which uses the [core part](https://bitbucket.org/ok-test-autoteam/tflr-core "Core part of the TFLR project") to generate reports on the current project.

Version: **1.0.2**

Compatible IDEA builds: **173.0+ (2017.3.5)**

## How to build
1. Load project from repository and launch in IDEA;
2. Choose "Import gradle settings" and select "Use gradle 'wrapper' task configuration";
3. To start new IDE instance with installed plugin (View -> Tool windows -> Gradle -> tflr-plugin -> Tasks -> intellij -> runIde);
4. Look for the tool window tab "TFLR" at the bottom of workspace to start using the plugin.

## How to use

Look for the tool window tab "TFLR" at the bottom of workspace to start using the plugin. Plugin can check current file or all files in the project, but checks will run only on test classes.

On "Options" tab you can choose your own paths to test files or config. For multiple choice you should separate paths with comma.

## Default paths and config

Default path for all project files (from project root directory): */src*

Default path and name for config (from project root directory): */tflr-config.yml*

[Example of config file](https://bitbucket.org/kongressman/test/src/master/tflr-plugin/tflr-config.yml).

## IntelliJ Theme

![](https://bitbucket.org/kongressman/test/raw/a5da85090b39358cb33b4f8624145d6688255654/Screenshots/IntelliJ%20Theme%2001.png)

![](https://bitbucket.org/kongressman/test/raw/a5da85090b39358cb33b4f8624145d6688255654/Screenshots/IntelliJ%20Theme%2002.png)


## Darcula Theme

![](https://bitbucket.org/kongressman/test/raw/a5da85090b39358cb33b4f8624145d6688255654/Screenshots/Darcula%20Theme%2001.png)

![](https://bitbucket.org/kongressman/test/raw/a5da85090b39358cb33b4f8624145d6688255654/Screenshots/Darcula%20Theme%2002.png)